var FormEditable = function() {

    $.mockjaxSettings.responseTime = 500;

    var log = function(settings, response) {
        var s = [],
            str;
        s.push(settings.type.toUpperCase() + ' url = "' + settings.url + '"');
        for (var a in settings.data) {
            if (settings.data[a] && typeof settings.data[a] === 'object') {
                str = [];
                for (var j in settings.data[a]) {
                    str.push(j + ': "' + settings.data[a][j] + '"');
                }
                str = '{ ' + str.join(', ') + ' }';
            } else {
                str = '"' + settings.data[a] + '"';
            }
            s.push(a + ' = ' + str);
        }
        s.push('RESPONSE: status = ' + response.status);

        if (response.responseText) {
            if ($.isArray(response.responseText)) {
                s.push('[');
                $.each(response.responseText, function(i, v) {
                    s.push('{value: ' + v.value + ', text: "' + v.text + '"}');
                });
                s.push(']');
            } else {
                s.push($.trim(response.responseText));
            }
        }
        s.push('--------------------------------------\n');
        $('#console').val(s.join('\n') + $('#console').val());
    }

    var initAjaxMock = function() {
        //ajax mocks

        $.mockjax({
            url: '/post',
            response: function(settings) {
                log(settings, this);
            }
        });

        $.mockjax({
            url: '/error',
            status: 400,
            statusText: 'Bad Request',
            response: function(settings) {
                this.responseText = 'Please input correct value';
                log(settings, this);
            }
        });

        $.mockjax({
            url: '/status',
            status: 500,
            response: function(settings) {
                this.responseText = 'Internal Server Error';
                log(settings, this);
            }
        });

        $.mockjax({
            url: '/groups',
            response: function(settings) {
                this.responseText = [{
                    value: 0,
                    text: 'Guest'
                }, {
                    value: 1,
                    text: 'Service'
                }, {
                    value: 2,
                    text: 'Customer'
                }, {
                    value: 3,
                    text: 'Operator'
                }, {
                    value: 4,
                    text: 'Support'
                }, {
                    value: 5,
                    text: 'Admin'
                }];
                log(settings, this);
            }
        });

    }

    var initEditables = function() {

        //set editable mode based on URL parameter
        if (App.getURLParameter('mode') == 'inline') {
            $.fn.editable.defaults.mode = 'inline';
            $('#inline').attr("checked", true);
        } else {
            $('#inline').attr("checked", false);
        }

        //global settings 
        $.fn.editable.defaults.inputclass = 'form-control';
        $.fn.editable.defaults.url = '/post';

        //editables element samples
        $('#nombreComercial').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'nombreComercial',
            title: 'Nombre comercial'
        });

        $('#edit-1').click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            $('#nombreComercial').editable('toggle');
        });

        $('#email').editable({
            url: '/post',
            type: 'email',
            pk: 1,
            name: 'email',
            title: 'Correo electrónico'
        });

        $('#edit-2').click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            $('#email').editable('toggle');
        });

         $('#url').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'url',
            title: 'Página web'
        });

        $('#edit-3').click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            $('#url').editable('toggle');
        });

         $('#state').editable({
            inputclass: 'form-control input-medium',
            source: ["Aguascalientes","Baja California", "Baja California Sur", "Campeche", "Ciudad de México", "Chiapas","Chihuahua","Coahuila","Colima","Durango","Estado de México","Guanajuato","Guerrero","Hidalgo","Jalisco","Michoacán","Morelos","Nayarit","Nuevo León","Oaxaca","Puebla","Querétaro","Quintana Roo","San Luis Potosí","Sinaloa","Sonora","Tabasco","Tamaulipas","Tlaxcala","Veracruz","Yucatán","Zacatecas"]
        });

          $('#edit-4').click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            $('#state').editable('toggle');
        });

          $('#delegacion').editable({
            inputclass: 'form-control input-medium',
            source:["Álvaro Obregón", "Azcapotzalco", "Benito Juárez", "Coyoacán", "Cuajimalpa de Morelos", "Cuauhtémoc","Gustavo A. Madero","Iztacalco", "Iztapalapa", "Magdalena Contreras", "Miguel Hidalgo", "Milpa Alta", "Tláhuac", "Tlalpan","Venustiano Carranza","Xochimilco"]
        });

        $('#edit-5').click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            $('#delegacion').editable('toggle');
        });

        $('#colonia').editable({
            inputclass: 'form-control input-medium',
            source:["10 de Abril", "16 de Septiembre", "5 de Mayo", "Agricultura","Ahuehuetes Anáhuac","América","Ampliación Daniel Garza","Ampliación Granada","Ampliación Popo","Ampliación Torre Blanca","Anáhuac I Sección","Anáhuac II Sección","Anzures","Argentina Antigua","Argentina Poniente","Bosque de las Lomas","Cuauhtémoc Pensil","Daniel Garza","Deportivo Pensil","Dos Lagos","Escandón I Sección"]
        });

        $('#edit-6').click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            $('#colonia').editable('toggle');
        });

         $('#codigo').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'codigo',
            title: 'Código Postal'
        });

        $('#edit-7').click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            $('#codigo').editable('toggle');
        });


        $('#address').editable({
            url: '/post',
            value: {
                calle: "Insurgentes Sur",
                numExt: "1250",
                numInt: "4-B"
            },
            validate: function(value) {
                if (value.calle == '') return 'información necesaria';
            },
            display: function(value) {
                if (!value) {
                    $(this).empty();
                    return;
                }
                var html = '<b>' + $('<div>').text(value.calle).html() + '</b>, ' + 'No. Ext.: ' + $('<div>').text(value.numExt).html() + '  No. Int.: ' + $('<div>').text(value.numInt).html();
                $(this).html(html);
            }
        });

          $('#edit-8').click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            $('#address').editable('toggle');
        });
          

        $('#telefono').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'telefono',
            title: 'Teléfonos'
        });

        $('#edit-9').click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            $('#telefono').editable('toggle');
        });

       
         

        $('#comments').editable({
            showbuttons: 'bottom'
        });

        $('#note').editable({
            showbuttons: (App.isRTL() ? 'left' : 'right')
        });

        $('#pencil').click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            $('#note').editable('toggle');
        });

       
       

    }

    return {
        //main function to initiate the module
        init: function() {

            // inii ajax simulation
            initAjaxMock();

            // init editable elements
            initEditables();

            // init editable toggler
            $('#enable').click(function() {
                $('#user .editable').editable('toggleDisabled');
            });

            // init 
            $('#inline').on('change', function(e) {
                if ($(this).is(':checked')) {
                    window.location.href = 'form-editable.html?mode=inline';
                } else {
                    window.location.href = 'form-editable.html';
                }
            });

            // handle editable elements on hidden event fired
            $('#user .editable').on('hidden', function(e, reason) {
                if (reason === 'save' || reason === 'nochange') {
                    var $next = $(this).closest('tr').next().find('.editable');
                    if ($('#autoopen').is(':checked')) {
                        setTimeout(function() {
                            $next.editable('show');
                        }, 300);
                    } else {
                        $next.focus();
                    }
                }
            });


        }

    };

}();

jQuery(document).ready(function() {
    FormEditable.init();
});