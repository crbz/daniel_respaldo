jQuery(document).ready(function () {
    // HIGHCHARTS DEMOS

    // LINE CHART 1
    $('#highchart_1').highcharts({
        chart: {
            style: {
                fontFamily: 'Open Sans'
            }
        },
        title: {
            text: 'Auditoría de Mensajes',
            x: -20 //center
        },
        subtitle: {
            text: '1 Junio - 21 Junio',
            x: -20
        },
        xAxis: {
            categories: ['1 Jun', '6 Jun', '7 Jun', '8 Jun', '9 Jun', '10 Jun',
				'13 Jun', '14 Jun', '15 Jun', '16 Jun', '17 Jun', '20 Jun', '21 Jun']
        },
        yAxis: {
            title: {
                text: 'Cantidad'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
			}]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'BC_AMDOCS_SUBSCRIBE',
            data: [80189, 86348, 86418, 86447, 86466, 86488, 88440, 89235, 90012, 90012, 90012, 90461, 94828],
            color: '#9999ff',
		}, {
            name: 'EAI_ADAPTOR_OUTBOUND',
            data: [14362, 14365, 14365, 14366, 14367, 14412, 14412, 14413, 14413, 14413, 14413, 14418, 14420],
            color: '#333399',
		}, {
            name: 'CYP_AMDOCS_SUBSCRIBE',
            data: [9498, 9718, 9754, 9793, 9835, 9858, 9913, 9953, 9987, 9987, 9987, 10059, 10092],
            color: '#336699',
		}, {
            name: 'OM_AMDOCS_SUBSCRIBE',
            data: [3442, 3526, 3572, 3612, 3636, 3644, 3666, 3955, 3997, 3997, 3997, 4122, 4140],
            color: '#33ccff',
		}, {
            name: 'CP_AMDOCS_SUBSCRIBE',
            data: [3018, 3027, 3044, 3044, 3044, 3044, 3044, 3592, 3597, 3597, 3597, 3616, 3616],
            color: '#009999',
		}, {
            name: 'AAM_AMDOCS_SUBSCRIBE',
            data: [1441, 1444, 1444, 1444, 1444, 1444, 1444, 1444, 1444, 1444, 1444, 1445, 1445],
            color: '#33cc66',
		}, {
            name: 'SFU_AMDOCS_SUBSCRIBE',
            data: [760, 793, 815, 830, 834, 839, 840, 849, 852, 852, 852, 892, 902],
            color: '#009900',
		}, {
            name: 'BC_INT_AMDOCS_SUBSCRIBE',
            data: [105, 105, 105, 105, 105, 105, 234, 234, 234, 234, 234, 234, 234],
            color: '#ffcc00',
		}, {
            name: 'ADPAMSS_ADAPTOR_OUTBOUND',
            data: [151, 151, 151, 151, 151, 151, 151, 151, 151, 151, 151, 151, 151],
            color: '#ff6600',
		}, {
            name: 'PF_AMDOCS_SUBSCRIBE',
            data: [63, 65, 67, 68, 67, 73, 94, 92, 92, 92, 92, 92, 92],
            color: '#993300',
		}, {
            name: 'PM_AMDOCS_SUBSCRIBE',
            data: [16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16],
            color: '#ff3333',
		}, {
            name: 'BC_AMDOCS_SERVICE',
            data: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            color: '#9933cc',
		}, {
            name: 'SP_AMDOCS_SUBSCRIBE',
            data: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            color: '#660066',
		}, {
            name: 'PF_INT_AMDOCS_SUBSCRIBE',
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            color: '#cc6699',
		}]
    });

    // LINE CHART 2
    $.getJSON('http://www.highcharts.com/samples/data/jsonp.php?filename=usdeur.json&callback=?', function (data) {

        $('#highchart_2').highcharts({
            chart: {
                zoomType: 'x',
                style: {
                    fontFamily: 'Open Sans'
                }
            },
            title: {
                text: 'USD to EUR exchange rate over time'
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                    'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: 'Exchange rate'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

            series: [{
                type: 'area',
                name: 'USD to EUR',
                data: data
            }]
        });
    });

    // AREA CHART
    $('#highchart_3').highcharts({
        chart: {
            type: 'area',
            style: {
                fontFamily: 'Open Sans'
            }
        },
        title: {
            text: 'Load Balancers'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['1750', '1800', '1850', '1900', '1950', '1999', '2050'],
            tickmarkPlacement: 'on',
            title: {
                enabled: false
            }
        },
        yAxis: {
            title: {
                text: 'Data'
            },
            labels: {
                formatter: function () {
                    return this.value / 1000;
                }
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ''
        },
        plotOptions: {
            area: {
                stacking: 'normal',
                lineColor: '#666666',
                lineWidth: 3,
                fillColor: 'transparent',
                marker: {
                    lineWidth: 0,
                    lineColor: '#666666'
                }
            }
        },
        series: [{
            name: '1',
            data: [3634, 635, 1009, 947, 1402, 2344, 5268],
            lineColor: '#99f',
            color: '#99f',
            marker: {
                fillColor: '#99f'
            }
        }, {
            name: '2',
            data: [106, 107, 111, 133, 221, 767, 1766],
            lineColor: '#3cf',
            color: '#3cf',
            marker: {
                fillColor: '#3cf'
            }
        }, {
            name: '3',
            data: [163, 203, 276, 408, 547, 729, 628],
            lineColor: '#3c6',
            color: '#3c6',
            marker: {
                fillColor: '#3c6'
            }
        }, {
            name: '4',
            data: [18, 31, 54, 156, 339, 818, 1201],
            lineColor: '#fc0',
            color: '#fc0',
            marker: {
                fillColor: '#fc0'
            }
        }, {
            name: '5',
            data: [2, 2, 2, 6, 13, 30, 46],
            lineColor: '#f60',
            color: '#f60',
            marker: {
                fillColor: '#f60'
            }
        }]
    });

    // BAR CHART
    // Age categories
    var categories = ['0-4', '5-9', '10-14', '15-19',
            '20-24', '25-29', '30-34', '35-39', '40-44',
            '45-49', '50-54', '55-59', '60-64', '65-69',
            '70-74', '75-79', '80-84', '85-89', '90-94',
            '95-99', '100 + '];

    $('#highchart_4').highcharts({
        chart: {
            type: 'bar',
            style: {
                fontFamily: 'Open Sans'
            }
        },
        title: {
            text: 'Population pyramid for Germany, 2015'
        },
        subtitle: {
            text: 'Source: <a href="http://populationpyramid.net/germany/2015/">Population Pyramids of the World from 1950 to 2100</a>'
        },
        xAxis: [{
            categories: categories,
            reversed: false,
            labels: {
                step: 1
            }
        }, { // mirror axis on right side
            opposite: true,
            reversed: false,
            categories: categories,
            linkedTo: 0,
            labels: {
                step: 1
            }
        }],
        yAxis: {
            title: {
                text: null
            },
            labels: {
                formatter: function () {
                    return Math.abs(this.value) + '%';
                }
            }
        },

        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + ', age ' + this.point.category + '</b><br/>' +
                    'Population: ' + Highcharts.numberFormat(Math.abs(this.point.y), 0);
            }
        },

        series: [{
            name: 'Male',
            data: [-2.2, -2.2, -2.3, -2.5, -2.7, -3.1, -3.2,
                -3.0, -3.2, -4.3, -4.4, -3.6, -3.1, -2.4,
                -2.5, -2.3, -1.2, -0.6, -0.2, -0.0, -0.0]
        }, {
            name: 'Female',
            data: [2.1, 2.0, 2.2, 2.4, 2.6, 3.0, 3.1, 2.9,
                3.1, 4.1, 4.3, 3.6, 3.4, 2.6, 2.9, 2.9,
                1.8, 1.2, 0.6, 0.1, 0.0]
        }]
    });

    // DONUT CHART
    var colors = Highcharts.getOptions().colors,
        categories = ['MSIE', 'Firefox', 'Chrome', 'Safari', 'Opera'],
        data = [{
            y: 56.33,
            color: colors[0],
            drilldown: {
                name: 'MSIE versions',
                categories: ['MSIE 6.0', 'MSIE 7.0', 'MSIE 8.0', 'MSIE 9.0', 'MSIE 10.0', 'MSIE 11.0'],
                data: [1.06, 0.5, 17.2, 8.11, 5.33, 24.13],
                color: colors[0]
            }
        }, {
            y: 10.38,
            color: colors[1],
            drilldown: {
                name: 'Firefox versions',
                categories: ['Firefox v31', 'Firefox v32', 'Firefox v33', 'Firefox v35', 'Firefox v36', 'Firefox v37', 'Firefox v38'],
                data: [0.33, 0.15, 0.22, 1.27, 2.76, 2.32, 2.31, 1.02],
                color: colors[1]
            }
        }, {
            y: 24.03,
            color: colors[2],
            drilldown: {
                name: 'Chrome versions',
                categories: ['Chrome v30.0', 'Chrome v31.0', 'Chrome v32.0', 'Chrome v33.0', 'Chrome v34.0',
                    'Chrome v35.0', 'Chrome v36.0', 'Chrome v37.0', 'Chrome v38.0', 'Chrome v39.0', 'Chrome v40.0', 'Chrome v41.0', 'Chrome v42.0', 'Chrome v43.0'
                    ],
                data: [0.14, 1.24, 0.55, 0.19, 0.14, 0.85, 2.53, 0.38, 0.6, 2.96, 5, 4.32, 3.68, 1.45],
                color: colors[2]
            }
        }, {
            y: 4.77,
            color: colors[3],
            drilldown: {
                name: 'Safari versions',
                categories: ['Safari v5.0', 'Safari v5.1', 'Safari v6.1', 'Safari v6.2', 'Safari v7.0', 'Safari v7.1', 'Safari v8.0'],
                data: [0.3, 0.42, 0.29, 0.17, 0.26, 0.77, 2.56],
                color: colors[3]
            }
        }, {
            y: 0.91,
            color: colors[4],
            drilldown: {
                name: 'Opera versions',
                categories: ['Opera v12.x', 'Opera v27', 'Opera v28', 'Opera v29'],
                data: [0.34, 0.17, 0.24, 0.16],
                color: colors[4]
            }
        }, {
            y: 0.2,
            color: colors[5],
            drilldown: {
                name: 'Proprietary or Undetectable',
                categories: [],
                data: [],
                color: colors[5]
            }
        }],
        browserData = [],
        versionsData = [],
        i,
        j,
        dataLen = data.length,
        drillDataLen,
        brightness;


    // Build the data arrays
    for (i = 0; i < dataLen; i += 1) {

        // add browser data
        browserData.push({
            name: categories[i],
            y: data[i].y,
            color: data[i].color
        });

        // add version data
        drillDataLen = data[i].drilldown.data.length;
        for (j = 0; j < drillDataLen; j += 1) {
            brightness = 0.2 - (j / drillDataLen) / 5;
            versionsData.push({
                name: data[i].drilldown.categories[j],
                y: data[i].drilldown.data[j],
                color: Highcharts.Color(data[i].color).brighten(brightness).get()
            });
        }
    }

    // Create the chart
    $('#highchart_5').highcharts({
        chart: {
            type: 'pie',
            style: {
                fontFamily: 'Open Sans'
            }
        },
        title: {
            text: 'Browser market share, January, 2015 to May, 2015'
        },
        subtitle: {
            text: 'Source: <a href="http://netmarketshare.com/">netmarketshare.com</a>'
        },
        yAxis: {
            title: {
                text: 'Total percent market share'
            }
        },
        plotOptions: {
            pie: {
                shadow: false,
                center: ['50%', '50%']
            }
        },
        tooltip: {
            valueSuffix: '%'
        },
        series: [{
            name: 'Browsers',
            data: browserData,
            size: '60%',
            dataLabels: {
                formatter: function () {
                    return this.y > 5 ? this.point.name : null;
                },
                color: '#ffffff',
                distance: -30
            }
        }, {
            name: 'Versions',
            data: versionsData,
            size: '80%',
            innerSize: '60%',
            dataLabels: {
                formatter: function () {
                    // display only if larger than 1
                    return this.y > 1 ? '<b>' + this.point.name + ':</b> ' + this.y + '%' : null;
                }
            }
        }]
    });



});